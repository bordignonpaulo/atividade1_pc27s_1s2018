
package udpsockets;

/**
 * Exemplo ilustrativo de comunicacao em rede com Sockets Java 
 * 
 * Autor: Paulo Henrique Bordignon
 * 
 * Ultima modificacao: 14/03/2018, 21:45
 *
 */
import java.io.*;
import java.net.*;
import java.util.Random;

class UDPClient {

    public UDPClient(){
        try{
        BufferedReader inFromUser
                = new BufferedReader(new InputStreamReader(System.in));
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName("localhost");
        byte[] sendData = new byte[1024];
        byte[] receiveData = new byte[1024];
        System.out.println("Cliente: ");
        String sentence = inFromUser.readLine();
        for (int i=0; i<1000; i++){
        sendData = sentence.getBytes();           
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 9876);
        //Dados do cliente estao inteiramente contidos no pacote
        //Servidor usara os dados do pacote para responder no socket do cliente
        clientSocket.send(sendPacket);            
            String resposta = setTimeout();
            if (Integer.parseInt(resposta)<2){
            System.out.println("Expirou tempo");
            }else {
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocket.receive(receivePacket); //note que a recepção eh feita no socket cliente
        String modifiedSentence = new String(receivePacket.getData());
        System.out.println("FROM SERVER:" + modifiedSentence);
            }        
        clientSocket.close();
        } // fim do for
        }catch (Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
        
    public static void main(String args[]) {
           UDPClient minhaApp = new UDPClient();
    }    
     public String setTimeout(){
        String resposta = "";
        try{
        Thread timeout = new Thread();
            Random tempo = new Random();
            int x = tempo.nextInt();
            resposta = x + "";
            timeout.sleep(x);            
        } catch (Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
        }        
        return resposta;
    }
}